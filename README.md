# MySQL Backend

## Description

This deployment aims to create a simple MySQL 8 instance via ArgoCD in order to provide a backend for any app that requires such backend.

## Files & directories

Name                                       | Type      | Description                          | Dependencies
-------------------------------------------|-----------|--------------------------------------|-------------------------------------------
default                                    | Directory | Contains de default deployment files | None
[deployment.yml](default/deployment.yml)   | File      | Main deployment                      | [persistence.yml](default/persistence.yml)
[persistence.yml](default/persistence.yml) | File      | Persistente Volume and Claim         | None
[service.yml](default/service.yml)         | File      | Servie to expose MySQL               | [deployment.yml](default/deployment.yml)

## Pre-Requisites

Create a Secret called `mysql-pass` like this:

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: mysql-pass
  namespace: ops-it
type: Opaque
stringData:
  password: <SOME PASSWORD HERE>

```